---
id: doc7
title: Informações básicas sobre Docker
sidebar_label: Instalação e comandos básicos de Doker
---


# Funcionamento e Instalação do Docker

Primeiro temos que definir o que não é Docker. Docker ![Docker](https://www.docker.com/sites/default/files/d8/styles/role_icon/public/2019-07/Docker-Logo-White-RGB_Vertical-BG_0.png?itok=8Tuac9I3) não é um sistema de virtualização tradicional. Enquanto em um ambiente de virtualização tradicional nós temos um S.O. completo e isolado, dentro do Docker nós temos recursos isolados que utilizando bibliotecas de kernel em comum (entre host e container), isso é possível pois o Docker utiliza como backend o nosso conhecido LXC.

## Docker + Microserviços
O Docker é bastante utilizado quando se trata de ferramentas voltadas a microserviços. Ele basicamente virtualiza uma máquina utilizando uma imagem (baixada do docker hub) mas utiliza partes do sistema. Essas partes podem ser utilizadas para rodar front, backend, BD separadamente em **containers**.

![Docker Logo](https://tr3.cbsistatic.com/hub/i/r/2016/10/18/831f017c-ee68-4bd6-8a5c-ab31b4d35d6d/resize/1200x/1d727d94737ac8571d079efce9a035af/dockerhero.jpg)

## Instalação

### Requisitos para instalação do Docker:
* Só funciona em processadores com arquitetura 64x.
* Versão do kernel acima de 3.8
* O Kernel deve ter módulos para suportar storages drivers e habilitado Cgroup Namespaces

```
#Verifique a versão do Kernel
$ uname -r
# Instalar com curl
$ curl -fsSL https://get.docker.com/ | sh
``` 
Para verificar se o Docker foi instalado, digite:
```
$ docker --version
```
Caso apareça a versão de instalação, **parabéns**! Seu Docker foi instalado com sucesso!!
De o Start na aplicação com o comando Systemctl:
```
$ sudo systemctl start docker 

#aceita os parâmetros status, start, stop, pause
```

##  docker --help
Comandos úteis:

```
$ docker ps #Visualizar os comandos em execução

$ docker run <imagem> #Comando para startar um container

$ docker images #mostrar as imagens de SO's disponíveis em sua máquina para uso, 
como se fossem .iso

$ docker ps -a #mostra todos os containers que foram parados ou finalizados com erro ou não

$ docker ps #mostra containers ativos

$ docker run -ti <image> /bin/bash #Roda um container com a imagem 
selecionada e abrirá o Shell para digitarmos os códigos

# Dentro do shell aperte Ctrl + P + Q para sair e deixar o container em execução

# Dentro do shell aperte Ctrl + D

$ docker attach <container ID> #para conectar a um determinado container

$ docker create <image> #Somente criar mas não conectar

$ docker start <Container ID> #Iniciar um container parado

$ docker pause <Container ID> #Pausar um container

$ docker unpause <Container ID> #Despausar container

$ docker stats <Container ID> #Visualizar recursos e status do container

$ docker top <Container ID> #Visualizar os processos que estão rodando no container

$ docker logs <Container ID> #Logs do container

$ docker rm <Container ID> #Remover container 

$ docker rm -f <Container ID> #Forçar a remoção do container
```


