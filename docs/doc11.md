---
id: doc11
title: Ansible
sidebar_label: Ansible Study
---

## Ansible

## O que é o Ansible?

https://github.com/matheusLeaao/ansible-study.git

Ferramenta utilizada para automação de instalação de programas em uma determinada infraestrutura.

Você consegue realizar instalação de serviços, start/stop/restart dos mesmos, integrar máquinas rodando remotamente arquivos YAML, JSON denominados de Ansible Playbooks (receitas) configuradas para realizar roles com intruções.

### Comando principal para rodar a receita

```
ansible-playbook main.yml -i hosts
```

## Exemplo de Ansible Playbook

### hosts (.txt)

```
[server]
localhost

#apagar comentários caso for rodar
#endereço que será rodado a receita. Será um endereço IPv4 caso seja rodado remotamente

[server:vars]
#env vars utilizadas como chamadas internas da receita

ansible_user=centos
ansible_ssh_private_key_file = /etc/ansible/liondelta.pem

#é utilizado ansible_ssh_private_key_file apontando para a chave de acesso SSH

```

### main.yml

Esse arquivo irá ser reponsável pela chamada das roles para poder rodar as tasks.

```
---
- hosts: server #nome do módulo configurado no hosts
  become: yes
  user: centos #usuário de acesso da máquina (root, ubuntu, centos)
  roles:
    - nome
    - das
    - roles
    - configuradas
    - para
    - serem
    - chamadas
```

Dentro das pastas que foram chamadas, elas possuem um arquivo main.yml que o Ansible reconhecerá como arquivo principal para ser rodado.

Veja por exemplo como é instalado arquivo comuns do linux:

```
---
- name: instalar padrão completo
  yum:
    name: "{{ packages }}"
  vars:
    packages:
    - vim
    - git
    - curl
    - MySQL-python
```

### Handlers

É utilizado para realizar start/stop/restart de determinado serviço. Para ser chamado é utilizado o "notify"

#### Chamada do handler realizada na instalação do Nginx

```
notify:
  - restart nginx
tags: nginx
```

#### main.yml da pasta handlers do Nginx

```
---
- name: restart nginx
  service:
    name: nginx
    state: restarted
    enabled: yes

#utilizando o módulo shell
- name: start nginx
  shell: sudo systemctl start nginx
  tags: nginx
```