---
id: doc6
title: Código final da Pipeline do Deploy da Doclion
sidebar_label: Pipeline Doclion
---


**Identação Obrigatória para o funcionamento da PipeLine**


```
image: node

stages:
  - test
  - package
  - deploy

test site:
  stage: test
  before_script:
    - npm install --global broken-link-checker@^0.7.8 wait-on@^2.1.0
  script:
    - cd website
    - npm install
    - npm run start &
    - wait-on http://doclion.surge.sh/ --timeout 90000
    - blc --recursive --exclude-external http://doclion.surge.sh/

vulnerabilites check: 
  stage: test # é marcado com a mesma tag (stage) para ocorrer em paralelo com o stage com nome igual
  script:
    - cd website 
    - npm install
    - npm run scan

build site:
  stage: package

  cache:
    key: pacote-site
    policy: push
    paths:
      - ./website

  #artifacts:
  #  name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
  #  when: always
  #  expire_in: 2h20min
  #  paths:
  #    - ./website/build/cursodevops
      #- ./home/matheusleao/Arquivos Importantes/git/gitlab/docusaurus/website/build/cursodevops

  script:
  - cd website
  - npm install
  - npm run build

deploy to production: &deploy
  stage: deploy
  variables:
    CNAME: doclion.surge.sh
    GIT_STRATEGY: none
  cache:
    key: pacote-site
    policy: pull
    paths:
      - ./website
  script:
    - npm install --global surge@^0.20.1
    - surge --project ./website/build/cursodevops --domain ${CNAME}
  environment:
    name: production
    url:  http://${CNAME}
  only: 
    - master

deploy to staging: 
  <<: *deploy #faz referencia ao &deploy,e stá herdando os dados e parâmetros do "deploy to production"
  variables:
    CNAME: staging-doclion.surge.sh
  environment:
    name: staging
  only: 
    - develop
```
