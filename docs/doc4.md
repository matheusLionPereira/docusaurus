---
id: academia
title: Academia dos Especialistas
sidebar_label: Academia dos Especialistas
---

![Academia dos Especialistas!!](img\academiaespecialistas.png)
![Academia](https://muc.mandic.com.br/hosp/novo_site_especialistas/imagens/prog-estagio-mandic-cor-01.png)

&nbsp;
# O que é o Programa e seus conceitos?

A disciplina é uma das habilidades mais importantes e úteis que todos devem possuir. Ela é essencial em todas as áreas da vida, tanto na pessoal quanto profissional, e, embora a maioria das pessoas reconheça sua importância, poucas se esforçam verdadeiramente para colocá-la em prática.

Uma das principais características de quem tem disciplina é a capacidade de abrir mão de momentos instantâneos de prazer, em favor de algum ganho maior, que precisa de esforço e tempo para ser realizado. Como um estudante que escolhe ficar em casa se preparando para uma prova ao invés de sair para se divertir com os amigos

A disciplina é um dos ingredientes mais importantes do sucesso. As características de uma pessoa que tem disciplina são:

* Ser perseverante.
* Capacidade de não desistir, apesar das falhas e dos contratempos.
* Ter autocontrole.
* Capacidade de resistir a distrações ou tentações.
* Tentar várias vezes até conseguir realizar o que você se propôs a fazer.

Tão importante quanto a disciplina, a iniciativa será sua aliada durante seu processo de formação na Academia de Especialistas. Afinal, quando quando se entende a importância da iniciativa, e estabelece isso para você, a vida ganha novos significados, novos caminhos. O que parecia difícil, complicado; se torna fácil, possível. É um princípio básico do universo, como Newton evidenciou na Terceira Lei: "para toda força de ação existe uma força de reação".

Imagine se Albert Einstein não tivesse estudado física e descoberto que grandes objetos (terra, sol e etc.) deformam o espaço, criando assim a gravidade que é a força necessária para existirem as órbitas? Talvez não existiriam satélites, o que consequentemente tornaria inviável o telefone celular como conhecemos. Com isso Steve Jobs não teria criado o iPhone, que foi responsável pela criação de um novo mercado que é o de aplicativos. Que gera receita de bilhões ao ano, milhares de empregos e consequentemente novos mercados, como o Uber está fazendo.

Com isto posto, a Academia de Especialistas lhe dá boas-vindas e o convida a empregar todo o seu potencial durante seu processo de formação, construindo conhecimento e aplicando-o na prática.

## POC

O primeiro passo é estudar um conjunto de temas separados para cada mês e preparar uma PoC (Prova de Conceito) a partir do que foi aprendido para um case proposto.

Uma prova de conceito, ou PoC (sigla do inglês, Proof of Concept) é um termo utilizado para denominar um modelo prático que possa provar o conceito (teórico) estabelecido por uma pesquisa ou artigo técnico. Pode ser considerado também uma implementação, em geral resumida ou incompleta, de um método ou de uma ideia, realizada com o propósito de verificar que o conceito ou teoria em questão é suscetível de ser explorado de uma maneira útil.

A PoC é considerada habitualmente um passo importante no processo de criação de um protótipo realmente operativo.

### Desafio POC 1º Mês

Construa um projeto que permita a empresa Solzil conectar seu escritório e a fazenda para troca de informações em tempo real com o melhor custo-benefício possível.

Você deve usar (minimamente) os conceitos aprendidos no curso acima, mas também poderá valer-se de outros conceitos não explorados para montar seu projeto.

O conteúdo deve estar em formato PPT e deve ser apresentado em 10 minutos.

### Desafio POC 2º Mês

Apresente um projeto de Desenvolvimento de uma aplicação em Python que atenderá a uma necessidade da Solzil

Você deve usar (minimamente) os conceitos aprendidos no curso acima, mas também poderá valer-se de outros conceitos não explorados para montar seu projeto.

O conteúdo deve estar em formato PPT e deve ser apresentado em 10 minutos.

### Desafio POC 3º Mês

Contextualizar Big Data, Data Science, Data Lake e Data Mining e apontar suas possíveis aplicações na Solzil.

Você deve usar (minimamente) os conceitos aprendidos no curso acima, mas também poderá valer-se de outros conceitos não explorados para montar seu projeto.

O conteúdo deve estar em formato PPT e deve ser apresentado em 10 minutos.

