---
id: doc9
title: Docker Compose
sidebar_label: Docker Compose
---

# docker-compose.yml

É uma maneira de usar containers no ambiente da aplicação e não somente as unidades (como funciona o Dockerfile). São utilizados em conjunto.
O docker Compose é possível criar vários containers em um mesmo arquivo .yml com muitos detalhes de configurações.

## Exemplo de docker_compose.yml

```
# build = Indica o caminho do seu Dockerfile
build: .

#command = Executa um comando 
command: bundle exec thin -p 3000

#container_name = Nome do container
container_name: my-web-container

#dns = Indica o dns server
dns: 8.8.8.8

#dns_search = Especifica um search domain
dns_search: example.com

#dockerfile = Especifica um Dockerfile alternativo.
dockerfile: Dockerfile-alternate

#env_file = Especifica um arquivo com variáveis de ambiente
env_file: .env

#environment = Adiciona variáveis de ambiente
environment:
  RACK_ENV: development

#expose = Expõe a porta do container
expose:
  - "3000"
  - "8000"

#external_links = "Linka" containers que não estão especificados no docker-compose atual
external_links:
  - redis_1
  - project_db_1:mysql

#extra_hosts = Adiciona uma entrada no /etc/hosts do container
extra_hosts:
  - "somehost:162.242.195.82"
  - "otherhost:50.31.209.229"

#image = Indica uma imagem
image: ubuntu:14.04

#labels = Adiciona metadata ao container
labels:
  com.example.description: "Accounting webapp"
  com.example.department: "Finance"

#links = Linka container dentro do mesmo docker-compose
links:
  - db
  - db:database

#log_driver = Indica o formato do log a ser gerado, por ex: syslog, json-file, etc
log_driver: syslog

OU

loggin:
driver: syslog

# log_opt = Indica onde mandar os logs, pode ser locar ou em um syslog remoto
log_opt:
  syslog-address: "tcp://192.186.0.42:123"

OU

loggin:
  driver: syslog
  options:
    syslog-address: "tcp://192.168.0.42.123"

#net = Modo de uso da rede
net: "bridge"
net: "host"

#ports = Expõe as portas do container e do host
ports:
  - "3000"
  - "8000:8000"
#volumes, volume_driver = Monta volumes dentro no container
volumes:
  # Apenas especifique o caminho e deixe o Engine criar o volume
  - /var/lib/mysql

  # Especifique um caminho absoluto mapeado
  - /opt/data:/var/lib/mysql

  # Caminho para o host, relativo para o arquivo compose
  - ./cache:/tmp/cache

#volumes_from = Monta volumes através de outro container
volumes_from:
  - service_name
  - service_name:ro

```