---
id: doc8
title: Container Docker
sidebar_label: Container Docker
---


  

# Docker

  

Docker é uma plataforma Open Source escrito em Go, que é uma linguagem de programação de alto desempenho desenvolvida dentro do Google, que facilita a criação e administração de ambientes isolados. Aprenda as principais características, instalação e configurações do Docker.

## Criar Container
**É necessário que você esteja em modo root**
```
$ sudo su
```
O Docker trabalha com imagens, como se fossem .iso, você pode criar sua própria imagem com determinado sistema (Dockerfile) ou importar diretamente do [Docker Hub](https://hub.docker.com/).


```
$ docker run -ti <imagem do SO>
```
**Pronto, você já está no container**

Para maiores informações e comandos para utilização do Docker, entre no [Informações básicas sobre Docker](http://doclion.surge.sh/docs/doc6).

Escreva somente o nome do Sistema Operacional desejado. Caso não tenha a imagem na biblioteca: não se preocupe! O Docker importa automaticamente caso o próprio sistema detecte que não há a imagem escolhida.


## Utilização do Docker Hub
O Docker Hub é um repositório de imagens criadas por nós para uso da comunidade. Possui imagens oficiais e não oficiais. Podemos upar nossas próprias imagens prontas utilizando o método pull-push do Git.
### Como utilizar?
* Primeiro crie uma conta no Docker Hub
* Faça login via linha de comando com seu usuário e senha criada no site
```
$ docker login
Login: usuário
Senha:
```
Depois de criado a conta e efetuado o login:

```
$ docker tag <container ID> usuário/repositório
```
Logo em seguida, execute o push da imagem para o repositório:

```
$ docker push usuário/repositório
```

Pronto, entre no site e vejo o repositório criado ou (via console):

```
$ docker login #O login será automático após o primeiro login
$ docker search usuário

NAME                    DESCRIPTION                                   STARS               OFFICIAL            AUTOMATED
usuário/repositório   Imagem exportada	   								0                                  
```

### Pull da imagem

Podemos também realizar o pull da imagem. Assim como no Git, temos que passar onde exatamente está a imagem.

```
$ docker pull usuário/repositório
$ docker images | grep <nome da imagem>
<nome_da_imagem>                  <version>                 <image ID>        3 hours ago         <size_in_MB>
```

### Exemplo de Dockerfile

O Dockerfile funciona como uma pipeline. Nele vamos pré-definir como o container irá subir, suas configurações, apps instalados, qual imagem irá utilizar, com qual container terá comunicação, o que precisarmos! Como padrão, todo arquivo dockerfile deve ter o nome assim mesmo: **Dockerfile**.

Devemos rodar o comando:
```
docker run -ti <nome_da_imagem> .
```
No diretório onde está localizado o arquivo **Dockerfile**
```
FROM ubuntu (nome da imagem)
MAINTAINER <nome> <email>

RUN apt-get update && apt-get clean <comandos a serem passados, concatenados com &&>
ADD opa.txt /diretorio/
CMD ["sh","-c","echo","$HOME"]
LABEL Description="Bla bla bla bla testando"
COPY ops.txt /diretorio/
ENTRYPOINT ["/usr/bin/apache2ctl","-D","FOREGROUND"]
ENV meunome="nome"
EXPOSE 80 <abrindo a porta 80, caso seja um container dedicado a http
USER <usuário>
WORKDIR /pasta_criada
VOLUME /diretorio
```
Os comandos reservados precisam estar em maiúsculo, questão de sintaxe.

