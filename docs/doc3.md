---
id: doc3
title: Apresentação POC Academia dos Especialistas
sidebar_label: Desafios POC
---
&nbsp;

# Academia dos Especialistas

A Academia dos Especialistas é um programa criado pela Mandic Cloud Solutions com o intuito de treinar o estagiário e deixá-lo preparado para a rotina de trabalho da empresa. São passados três desafio para serem executados com base em seis cursos em 3 meses.

## Desafio 1º Mês

* Construa um projeto que permita a empresa Solzil conectar seu escritório e a fazenda para troca de
informações em tempo real com o melhor custo-benefício possível.
* Você deve usar (minimamente) os conceitos aprendido no curso acima, mas também poderá utilizar de outros conceitos não explorados para montar seu projeto.

## Desafio 2º Mês

* Apresente um projeto de Desenvolvimento de uma aplicação em Python que atenderá a uma necessidade da Solzil.
* Você deve usar (minimamente) os conceitos aprendidos no curso acim, mas também poderá valer-se de outros conceitos não explorados para montar seu projeto.

## Desafio 3º Mês

* Contextualizar Big Data, Data Science, Data Lake e Data Mining e apontar suas possíveis aplicações na Solzil.
* Você deve usar (minimamente) os conceitos aprendidos no curso acima, mas também poderá valer-se de outros conceitos não explorados para montar seu projeto.

### Projeto desenvolvido por mim

Esse é o link do projeto: 

https://docs.google.com/presentation/d/1tLpQEQBw_aVKmMOGz3V7ZL3kQjMdIB3ahNR89TzK9JI/edit?usp=sharing



