---
id: doc5
title: Surge.sh
sidebar_label: Funcionamento do Surge.sh!
---

# Ferramenta Surge.sh

![imagem surge](https://surge.sh/images/logos/svg/surge-logo.svg "Surge.sh")  

Publicação estática na web para devs Front-End

Simples, com um só comando de publicação web. Publique HTML, CSS e JS **de graça**, sem deixar a linha de comando.

**É NECESSÁRIO QUE VOCÊ TENHA INSTALADO O COMANDO "npm": GERENCIADOR DE DEPENDÊNCIAS DO NODE (Node Package Manager).**
```
npm install --global surge
# No diretório do seu projeto, apenas rode...
surge
```
![gif instalação](https://miro.medium.com/max/1000/1*2GUDElAqCQDokkpnB32Isw.gif "Gif da instalação do Surge")

Após rodar o comando "surge", o sistema irá pedir para que você entre com o diretório completo do projeto, normalmente onde está o package.json do seu projeto. Logo em seguida, digite o nome do domínio que você deseja dar para o seu projeto que ficará disponibilizado na web de forma free.

**É obrigatório que tenha, no final da URL, o complemento surge.sh.**

Exemplo:
```
$ surge
  
  project: caminho/até/meu-projeto
  domain: meu-projeto.surge.sh
  upload: [============]

Success! Published and running at meu-projeto.surge.sh
```
Para logar no sistema:
```
$ surge login
```
E entre com o seu login para criar seu acesso.

## Lista de comandos surge úteis 
```
$ surge --help

surge – comando simples para ter a publicação na web. (v0.21.3)

  Utilitários:
    surge <project> <domain>

  Opções:
    -a, --add           adiciona usuário na lista de colaboradores (endereço de email)
    -r, --remove        remove usuário da lista de colaboradores (endereço de email)
    -V, --version       mostra a versão do surge
    -h, --help          mostra esse documento de ajuda

  Comandos adicionais:
    surge whoami        mostra quem está logado
    surge logout        expira o login atual
    surge login         loga no surge
    surge list          lista todos os projetos que seu email possui acesso
    surge teardown      da baixa em determinado domínio escolhido
    surge plan          set account plan

  Guias:
    Como começar     surge.sh/help/getting-started-with-surge
    Domínios customizados      surge.sh/help/adding-a-custom-domain
    Ajudas adicionais     surge.sh/help
    
  Quando em dúvida, rode "surge" na pasta do diretório do seu projeto

```

Ao rodar o comando de publicação na web, o seu programa fica hospedado na web através da ferramenta open source Surge.sh


## De graça???

Sim! Totalmente gratuíto! Como?

Segundo a própria equipe do Surge, os desenvolvedores se preocupam bastante com a comunidade de desenvolvimento e com plataformas Open Source. Para eles, a ideia de que  **todos**  possam subir facilmente seus projetos para web é muito importante (você pode ler esse pequeno  [texto aqui](https://surge.sh/help/why-is-surge-free))


### Como funciona?


O seu funcionamento é bem simples. O Surge possui uma CLI — Command Line Interface ou em uma tradução livre, Interface de linha de comando criada em NodeJS.

Para assimilar melhor, pense em CLI como aqueles comandos que a gente roda no terminal para executar alguma ação como, por exemplo, o próprio GIT. A gente digita um comando (`git`) , passamos comandos e então e alguma será executada.

Assim, o requisito mínimo para você usar o  _Surge_  é você ter o  [NodeJS](https://nodejs.org/en/)  (e consequentemente o NPM) instalado na sua máquina e um terminalzinho.

Fonte:
[Medium](https://medium.com/trainingcenter/subindo-seu-projeto-front-surge-52d47cf31e0b)
