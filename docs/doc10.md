---
id: doc10
title: Jenkins + Docker
sidebar_label: Jenkins + Docker
---

# Jenkins + Docker

## O que é o Jenkins

![https://wiki.jenkins.io/download/attachments/2916393/logo.png?version=1&modificationDate=1302753947000&api=v2](https://wiki.jenkins.io/download/attachments/2916393/logo.png?version=1&modificationDate=1302753947000&api=v2)

Jenkins é um serviço open source de automação de servers programado em Java. Jenkins ajuda a automatização da parte não humana do processo de desenvolvimento de software, com CI e aspectos técnicos que facilitam o CD. É um sistema de base de servidor que roda no Java Servlet.
